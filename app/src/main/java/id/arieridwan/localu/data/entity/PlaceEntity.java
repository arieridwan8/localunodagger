package id.arieridwan.localu.data.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "place")
public class PlaceEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;
    private String description;
    private String wide_area;
    private String address;
    private float lat;
    private float lng;

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setWide_area(String wide_area) {
        this.wide_area = wide_area;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }

    public PlaceEntity(int id, String name, String description, String wide_area, String address, float lat, float lng) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.wide_area = wide_area;
        this.address = address;
        this.lat = lat;
        this.lng = lng;
    }

    public PlaceEntity(PlaceEntity placeEntity) {
        this.id = placeEntity.id;
        this.name = placeEntity.name;
        this.description = placeEntity.description;
        this.wide_area = placeEntity.wide_area;
        this.address = placeEntity.address;
        this.lat = placeEntity.lat;
        this.lng = placeEntity.lng;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getWide_area() {
        return wide_area;
    }

    public String getAddress() {
        return address;
    }

    public float getLat() {
        return lat;
    }

    public float getLng() {
        return lng;
    }

}

package id.arieridwan.localu.ui.addplace;

import android.arch.lifecycle.ViewModelProviders;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;
import id.arieridwan.localu.R;
import id.arieridwan.localu.data.db.PlaceDatabase;
import id.arieridwan.localu.ui.common.BaseActivity;

public class AddPlaceActivity extends BaseActivity {

    private final String DATABASE_NAME = "place_db";
    private final int PLACE_PICKER_REQUEST = 1;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.et_name)
    EditText mEtName;

    @BindView(R.id.til_name)
    TextInputLayout mTilName;

    @BindView(R.id.et_desc)
    EditText mEtDesc;

    @BindView(R.id.til_desc)
    TextInputLayout mTilDesc;

    @BindView(R.id.et_area)
    EditText mEtArea;

    @BindView(R.id.til_area)
    TextInputLayout mTilArea;

    @BindView(R.id.et_address)
    EditText mEtAddress;

    @BindView(R.id.til_address)
    TextInputLayout mTilAddress;

    @BindView(R.id.et_lat)
    EditText mEtLat;

    @BindView(R.id.til_lat)
    TextInputLayout mTilLat;

    @BindView(R.id.et_lng)
    EditText mEtLng;

    @BindView(R.id.til_lng)
    TextInputLayout mTilLng;

    @BindView(R.id.btn_submit)
    AppCompatButton mBtnSubmit;

    private PlaceDatabase placeDatabase;
    private AddPlaceViewModel mViewModel;

    @Override
    public int getResLayout() {
        return R.layout.activity_add_place;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initToolbar();
        initContentViews();
        initViewModel();
        openLocationPicker();
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Add place");
    }

    private void initContentViews() {
        mEtAddress.setOnClickListener((v)-> openLocationPicker());
        mEtLng.setOnClickListener((v)-> openLocationPicker());
        mEtLng.setOnClickListener((v)-> openLocationPicker());
    }

    private void openLocationPicker() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    private void initViewModel() {
        mViewModel = ViewModelProviders.of(this)
                .get(AddPlaceViewModel.class);

        placeDatabase = Room.databaseBuilder(getApplicationContext(),
                PlaceDatabase.class, DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build();

        mViewModel.initViewModel(placeDatabase.placeDao());
    }

    private boolean isDataValid() {
        boolean valid = true;

        String name = mEtName.getText().toString();
        String description = mEtDesc.getText().toString();
        String address = mEtAddress.getText().toString();
        String area = mEtArea.getText().toString();
        String lat = mEtLat.getText().toString();
        String lng = mEtLng.getText().toString();

        if (name.isEmpty() || name.length() < 3) {
            mTilName.setErrorEnabled(true);
            mTilName.setError("at least 3 characters");
            valid = false;
        } else {
            mTilName.setErrorEnabled(false);
            mTilName.setError(null);
        }

        if (description.isEmpty() || name.length() < 3) {
            mTilDesc.setErrorEnabled(true);
            mTilDesc.setError("at least 5 characters");
            valid = false;
        } else {
            mTilDesc.setErrorEnabled(false);
            mTilDesc.setError(null);
        }

        if (address.isEmpty() || name.length() < 3) {
            mTilAddress.setErrorEnabled(true);
            mTilAddress.setError("at least 5 characters");
            valid = false;
        } else {
            mTilAddress.setErrorEnabled(false);
            mTilAddress.setError(null);
        }

        if (area.isEmpty() || name.length() < 3) {
            mTilArea.setErrorEnabled(true);
            mTilArea.setError("do not empty");
            valid = false;
        } else {
            mTilArea.setErrorEnabled(false);
            mTilArea.setError(null);
        }

        if (lat.isEmpty() || name.length() < 3) {
            mTilLat.setErrorEnabled(true);
            mTilLat.setError("do not empty");
            valid = false;
        } else {
            mTilLat.setErrorEnabled(false);
            mTilLat.setError(null);
        }

        if (lng.isEmpty() || name.length() < 3) {
            mTilLng.setErrorEnabled(true);
            mTilLng.setError("do not empty");
            valid = false;
        } else {
            mTilLng.setErrorEnabled(false);
            mTilLng.setError(null);
        }

        return valid;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                if (place != null) {
                    mEtName.setText(place.getName());
                    mEtAddress.setText(place.getAddress());
                    mEtLat.setText(String.valueOf(place.getLatLng().latitude));
                    mEtLng.setText(String.valueOf(place.getLatLng().longitude));
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_submit)
    public void onSubmitClicked() {
        if (isDataValid()) {
            float lat = Float.parseFloat(mEtLat.getText().toString());
            float lng = Float.parseFloat(mEtLng.getText().toString());
            mViewModel.addPlace(mEtName.getText().toString().trim(), mEtDesc.getText().toString().trim(),
                    mEtAddress.getText().toString().trim(), mEtArea.getText().toString().trim(),
                    lat, lng)
            .observe(this, isSuccess->{
                if (isSuccess) {
                    Toast.makeText(this, "Succussfully saved", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(this, "Failed to save", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

}

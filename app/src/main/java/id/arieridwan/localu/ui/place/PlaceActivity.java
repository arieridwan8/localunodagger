package id.arieridwan.localu.ui.place;

import android.arch.lifecycle.ViewModelProviders;
import android.arch.persistence.room.Room;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import java.util.Objects;

import butterknife.BindView;
import id.arieridwan.localu.R;
import id.arieridwan.localu.data.db.PlaceDatabase;
import id.arieridwan.localu.ui.common.BaseActivity;

public class PlaceActivity extends BaseActivity {

    private final String DATABASE_NAME = "place_db";

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.rv_place)
    RecyclerView mRvPlace;

    private PlaceDatabase placeDatabase;
    private PlaceViewModel mViewModel;
    private PlaceRvAdapter mAdapter;

    @Override
    public int getResLayout() {
        return R.layout.activity_place;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initToolbar();
        initPlaces();
        initViewModel();
        setData();
    }

    private void initPlaces() {
        mAdapter = new PlaceRvAdapter();
        mRvPlace.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mRvPlace.setAdapter(mAdapter);
        mRvPlace.setHasFixedSize(true);
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Places");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    private void initViewModel() {
        mViewModel = ViewModelProviders.of(this)
                .get(PlaceViewModel.class);

        placeDatabase = Room.databaseBuilder(getApplicationContext(),
                PlaceDatabase.class, DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build();

        mViewModel.initViewModel(placeDatabase.placeDao());
    }

    private void setData() {
        mViewModel.getPlaces().observe(this, list -> {
            mAdapter.setPlaceList(list);
            mAdapter.notifyDataSetChanged();
        });
    }

}

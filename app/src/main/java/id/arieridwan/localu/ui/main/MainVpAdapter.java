package id.arieridwan.localu.ui.main;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import id.arieridwan.localu.data.entity.PlaceEntity;

public class MainVpAdapter extends FragmentPagerAdapter {

    private List<PlaceEntity> mList = new ArrayList<>();

    public MainVpAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return PlaceItemFragment.newInstance(mList.get(position).getName(), mList.get(position).getAddress());
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    public void setPlaces(List<PlaceEntity> mList) {
        this.mList = mList;
    }

    public PlaceEntity getPlace(int position) {
        return mList.get(position);
    }

}

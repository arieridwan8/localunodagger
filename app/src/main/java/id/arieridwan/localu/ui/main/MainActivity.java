package id.arieridwan.localu.ui.main;

import android.Manifest;
import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.content.res.Configuration;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import butterknife.BindView;
import butterknife.OnClick;
import id.arieridwan.localu.R;
import id.arieridwan.localu.data.db.PlaceDatabase;
import id.arieridwan.localu.ui.addplace.AddPlaceActivity;
import id.arieridwan.localu.ui.common.BaseActivity;
import id.arieridwan.localu.ui.login.LoginActivity;
import id.arieridwan.localu.ui.place.PlaceActivity;
import timber.log.Timber;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

public class MainActivity extends BaseActivity implements OnMapReadyCallback, ViewPager.OnPageChangeListener {

    private final String DATABASE_NAME = "place_db";
    private final String TAG = MainActivity.class.getSimpleName();
    private final long UPDATE_INTERVAL = 60000;
    private final long FASTEST_INTERVAL = 5000;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.nvView)
    NavigationView mNavView;

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    @BindView(R.id.vpPlace)
    ViewPager mVpPlace;

    private PlaceDatabase placeDatabase;
    private MainViewModel mViewModel;
    private SupportMapFragment mMapFrament;
    private GoogleMap mMap;
    private LocationRequest mLocationRequest;
    private Location mCurrentLocation;
    private ActionBarDrawerToggle drawerToggle;
    private MainVpAdapter mVpAdapter;

    @Override
    public int getResLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Places");
        mMapFrament = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mMapFrament.getMapAsync(this);
        setupDrawerContent();
        drawerToggle = setupDrawerToggle();
        mDrawerLayout.addDrawerListener(drawerToggle);
        initPlaces();
        initViewModel();
        getData();
    }

    private void initPlaces() {
        mVpAdapter = new MainVpAdapter(getSupportFragmentManager());
        mVpPlace.setAdapter(mVpAdapter);
        mVpPlace.addOnPageChangeListener(this);
        mVpPlace.setClipToPadding(false);
        mVpPlace.setPageMargin(24);
        mVpPlace.setPadding(48, 0, 48, 0);
        mVpPlace.setOffscreenPageLimit(3);
    }

    private void initViewModel() {
        mViewModel = ViewModelProviders.of(this)
                .get(MainViewModel.class);

        placeDatabase = Room.databaseBuilder(getApplicationContext(),
                PlaceDatabase.class, DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build();

        mViewModel.initViewModel(placeDatabase.placeDao());
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.drawer_open, R.string.drawer_close);
    }

    private void setupDrawerContent() {
        mNavView.setNavigationItemSelectedListener(
                menuItem -> {
                    selectDrawerItem(menuItem);
                    return true;
                });
    }

    private void selectDrawerItem(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.nav_add:
                Intent intent = new Intent(this, AddPlaceActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                return;
            case R.id.nav_view:
                Intent intent2 = new Intent(this, PlaceActivity.class);
                startActivity(intent2);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                return;
            case R.id.nav_logout:
                Intent intent3 = new Intent(getApplicationContext(), LoginActivity.class);
                intent3.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent3);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                return;

        }
        menuItem.setChecked(true);
        mDrawerLayout.closeDrawers();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.setOnMyLocationButtonClickListener(() -> {
            startLocationUpdates();
            return false;
        });
        checkLocationPermission();
    }

    @SuppressLint("MissingPermission")
    private void checkLocationPermission() {
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        getMyLocation();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        Toast.makeText(getApplicationContext(), R.string.permission_location_description, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        Toast.makeText(getApplicationContext(), R.string.permission_location_description, Toast.LENGTH_SHORT).show();
                    }
                })
                .check();
    }


    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("GPS is settings");
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");
        alertDialog.setPositiveButton("Settings", (dialog, which) -> {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        });
        alertDialog.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
        alertDialog.show();
    }

    @SuppressWarnings({"MissingPermission"})
    void getMyLocation() {
        FusedLocationProviderClient locationClient = getFusedLocationProviderClient(this);
        locationClient.getLastLocation()
                .addOnSuccessListener(location -> {
                    if (location != null) {
                        onLocationChanged(location);
                    }
                })
                .addOnFailureListener(e -> {
                    Timber.d("Error trying to get last GPS location");
                    e.printStackTrace();
                });
    }

    public boolean isGPSEnabled() {
        LocationManager locationManager = (LocationManager)
                getSystemService(LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mCurrentLocation != null) {
            LatLng latLng = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 17);
            mMap.animateCamera(cameraUpdate);
        }
        startLocationUpdates();
        mNavView.setCheckedItem(0);
        getData();
    }

    @SuppressLint("MissingPermission")
    protected void startLocationUpdates() {
        if (isGPSEnabled()) {
            mLocationRequest = new LocationRequest();
            mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
            mLocationRequest.setInterval(UPDATE_INTERVAL);
            mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
            builder.addLocationRequest(mLocationRequest);
            LocationSettingsRequest locationSettingsRequest = builder.build();

            SettingsClient settingsClient = LocationServices.getSettingsClient(this);
            settingsClient.checkLocationSettings(locationSettingsRequest);

            getFusedLocationProviderClient(this).requestLocationUpdates(mLocationRequest, new LocationCallback() {
                        @Override
                        public void onLocationResult(LocationResult locationResult) {
                            onLocationChanged(locationResult.getLastLocation());
                        }
                    },
                    Looper.myLooper());
        } else {
            showSettingsAlert();
        }
    }

    public void onLocationChanged(Location location) {
        if (location == null) {
            return;
        }
        mCurrentLocation = location;
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 17);
        mMap.animateCamera(cameraUpdate);
        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(location.getLatitude(), location.getLongitude()))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_me))
                .title("My Location"));
    }

    @OnClick(R.id.fab_my_location)
    public void onMyLocationClicked() {
        startLocationUpdates();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    private void getData() {
        mViewModel.getPlaces().observe(this, list -> {
            mVpAdapter.setPlaces(list);
            mVpAdapter.notifyDataSetChanged();
            for (int i = 0; i < list.size(); i++) {
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(list.get(i).getLat(), list.get(i).getLng()))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_place))
                        .title("My Location"));
            }
        });
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (mVpAdapter.getPlace(position) != null) {
            mVpAdapter.getPlace(position);
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(mVpAdapter.getPlace(position).getLat(),
                    mVpAdapter.getPlace(position).getLng()), 17);
            mMap.animateCamera(cameraUpdate);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

}

package id.arieridwan.localu.ui.main;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import id.arieridwan.localu.data.dao.PlaceDao;
import id.arieridwan.localu.data.entity.PlaceEntity;
import id.arieridwan.localu.repository.PlaceRepository;
import id.arieridwan.localu.repository.PlaceRepositoryImpl;

/**
 * Created by arieridwan on 13/05/18.
 */

public class MainViewModel extends ViewModel {

    PlaceRepository placeRepository;

    public MainViewModel() {
    }

    public void initViewModel(PlaceDao placeDao) {
        placeRepository = new PlaceRepositoryImpl(placeDao);
    }

    public LiveData<List<PlaceEntity>> getPlaces() {
        return placeRepository.getPlaces();
    }

}

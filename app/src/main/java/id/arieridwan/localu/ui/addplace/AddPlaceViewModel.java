package id.arieridwan.localu.ui.addplace;

import android.annotation.SuppressLint;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import id.arieridwan.localu.data.dao.PlaceDao;
import id.arieridwan.localu.data.entity.PlaceEntity;
import id.arieridwan.localu.repository.PlaceRepository;
import id.arieridwan.localu.repository.PlaceRepositoryImpl;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by arieridwan on 13/05/18.
 */

public class AddPlaceViewModel extends ViewModel {

    PlaceRepository placeRepository;

    public AddPlaceViewModel() {
    }

    public void initViewModel(PlaceDao placeDao) {
        placeRepository = new PlaceRepositoryImpl(placeDao);
    }

    @SuppressLint("CheckResult")
    MutableLiveData<Boolean> addPlace(String name, String description, String address, String wide_area, float lat, float lng) {

        MutableLiveData<Boolean> status = new MutableLiveData<>();
        PlaceEntity placeEntity = new PlaceEntity(0, name, description, wide_area, address, lat, lng);

        placeRepository.addPlace(placeEntity).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(()->  status.setValue(true), throwable -> status.setValue(false));

        return status;
    }

}

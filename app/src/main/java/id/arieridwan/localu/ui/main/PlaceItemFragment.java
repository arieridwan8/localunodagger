package id.arieridwan.localu.ui.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import id.arieridwan.localu.R;

public class PlaceItemFragment extends Fragment {

    private static final String EXTRA_NAME = "EXTRA_NAME";
    private static final String EXTRA_ADDRESS = "EXTRA_ADDRESS";

    @BindView(R.id.tv_name)
    TextView mTvName;
    @BindView(R.id.tv_address)
    TextView mTvAddress;

    private Unbinder unbinder;

    private String name;
    private String address;

    public static PlaceItemFragment newInstance(String name, String address) {
        PlaceItemFragment fragment = new PlaceItemFragment();
        Bundle args = new Bundle();
        args.putString(EXTRA_NAME, name);
        args.putString(EXTRA_ADDRESS, address);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        name = getArguments().getString(EXTRA_NAME);
        address = getArguments().getString(EXTRA_ADDRESS);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.place_item_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);
        mTvName.setText(name);
        mTvAddress.setText(address);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}

package id.arieridwan.localu.ui.place;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.arieridwan.localu.R;
import id.arieridwan.localu.data.entity.PlaceEntity;

public class PlaceRvAdapter extends RecyclerView.Adapter<PlaceRvAdapter.ViewHolder> {

    private List<PlaceEntity> mList = new ArrayList<>();

    public PlaceRvAdapter() {
    }

    public void setPlaceList(List<PlaceEntity> list) {
        this.mList = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.place_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PlaceEntity mData = mList.get(position);
        holder.mTvName.setText(mData.getName());
        holder.mTvAddress.setText(mData.getAddress());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_name)
        TextView mTvName;

        @BindView(R.id.tv_address)
        TextView mTvAddress;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

}

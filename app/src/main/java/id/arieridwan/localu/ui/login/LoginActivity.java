package id.arieridwan.localu.ui.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.OnClick;
import id.arieridwan.localu.R;
import id.arieridwan.localu.ui.common.BaseActivity;
import id.arieridwan.localu.ui.main.MainActivity;
import id.arieridwan.localu.ui.signup.SignupActivity;

public class LoginActivity extends BaseActivity {

    private final String TAG = LoginActivity.class.getSimpleName();

    @BindView(R.id.et_email)
    EditText mEtEmail;

    @BindView(R.id.et_password)
    EditText mEtPassword;

    @BindView(R.id.tv_signup)
    TextView mTvSignup;

    @BindView(R.id.btn_login)
    AppCompatButton mBtnLogin;

    @BindView(R.id.til_email)
    TextInputLayout mTilEmail;

    @BindView(R.id.til_password)
    TextInputLayout mTilPassword;

    @Override
    public int getResLayout() {
        return R.layout.activity_login;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initSignUpText();
    }

    private void initSignUpText() {
        String text = getResources().getString(R.string.login_text_signup);

        int indexStart = 16;
        int indexEnd = text.length();

        SpannableStringBuilder linkTermsConds = new SpannableStringBuilder(text);
        linkTermsConds.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorAccent)), indexStart, indexEnd, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        linkTermsConds.setSpan(new StyleSpan(Typeface.BOLD), indexStart, indexEnd, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        mTvSignup.setMovementMethod(LinkMovementMethod.getInstance());
        linkTermsConds.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
            }
        }, indexStart, indexEnd, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        mTvSignup.setText(linkTermsConds);
    }

    @OnClick(R.id.btn_login)
    public void onLoginClicked() {
        closeKeyboard();
        login();
    }

    public void login() {
        Log.d(TAG, "Login");

        if (!validate()) {
            onLoginFailed();
            return;
        }

        mBtnLogin.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();

        String email = mEtEmail.getText().toString();
        String password = mEtPassword.getText().toString();

        new Handler().postDelayed(() -> {
            // On complete call either onLoginSuccess or onLoginFailed
            onLoginSuccess();
            // onLoginFailed();
            progressDialog.dismiss();
        }, 3000);
    }

    public boolean validate() {
        boolean valid = true;

        String email = mEtEmail.getText().toString();
        String password = mEtPassword.getText().toString();

        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mTilEmail.setErrorEnabled(true);
            mTilEmail.setError("enter a valid email address");
            valid = false;
        } else {
            mTilEmail.setErrorEnabled(false);
            mTilEmail.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            mTilPassword.setErrorEnabled(true);
            mTilPassword.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            mTilPassword.setErrorEnabled(false);
            mTilPassword.setError(null);
        }

        return valid;
    }

    public void onLoginSuccess() {
        mBtnLogin.setEnabled(true);
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();
        mBtnLogin.setEnabled(true);
    }

    private void closeKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}

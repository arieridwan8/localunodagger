package id.arieridwan.localu.ui.signup;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;
import id.arieridwan.localu.R;
import id.arieridwan.localu.ui.common.BaseActivity;
import id.arieridwan.localu.ui.login.LoginActivity;
import id.arieridwan.localu.ui.main.MainActivity;
import timber.log.Timber;

public class SignupActivity extends BaseActivity {

    private final String TAG = SignupActivity.class.getSimpleName();

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.et_name)
    EditText mEtName;

    @BindView(R.id.til_name)
    TextInputLayout mTilName;

    @BindView(R.id.et_address)
    EditText mEtAddress;

    @BindView(R.id.til_address)
    TextInputLayout mTilAddress;

    @BindView(R.id.et_email)
    EditText mEtEmail;

    @BindView(R.id.til_email)
    TextInputLayout mTilEmail;

    @BindView(R.id.et_phone)
    EditText mEtPhone;

    @BindView(R.id.til_phone)
    TextInputLayout mTilPhone;

    @BindView(R.id.et_password)
    EditText mEtPassword;

    @BindView(R.id.til_password)
    TextInputLayout mTilPassword;

    @BindView(R.id.et_re_password)
    EditText mEtRePassword;

    @BindView(R.id.til_re_password)
    TextInputLayout mTilRePassword;

    @BindView(R.id.btn_signup)
    AppCompatButton mBtnSignup;

    @BindView(R.id.tv_login)
    TextView mTvLogin;

    @Override
    public int getResLayout() {
        return R.layout.activity_signup;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Signup");
        initLoginText();
    }

    private void initLoginText() {
        String text = getResources().getString(R.string.signup_text_login);

        int indexStart = 18;
        int indexEnd = text.length();

        SpannableStringBuilder linkTermsConds = new SpannableStringBuilder(text);
        linkTermsConds.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorAccent)), indexStart, indexEnd, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        linkTermsConds.setSpan(new StyleSpan(Typeface.BOLD), indexStart, indexEnd, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        mTvLogin.setMovementMethod(LinkMovementMethod.getInstance());
        linkTermsConds.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
            }
        }, indexStart, indexEnd, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        mTvLogin.setText(linkTermsConds);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_signup)
    public void onSignupClicked() {
        signup();
    }

    public void signup() {

        Timber.d("Signup");

        if (!validate()) {
            onSignupFailed();
            return;
        }

        mBtnSignup.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(SignupActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Creating Account...");
        progressDialog.show();

        String name = mEtName.getText().toString();
        String address = mEtAddress.getText().toString();
        String email = mEtEmail.getText().toString();
        String mobile = mEtPhone.getText().toString();
        String password = mEtPassword.getText().toString();
        String reEnterPassword = mEtRePassword.getText().toString();

        // TODO: Implement your own signup logic here.

        new android.os.Handler().postDelayed(() -> {
                    // On complete call either onSignupSuccess or onSignupFailed
                    // depending on success
                    onSignupSuccess();
                    // onSignupFailed();
                    progressDialog.dismiss();
                }, 3000);
    }

    public boolean validate() {
        boolean valid = true;

        String name = mEtName.getText().toString();
        String address = mEtAddress.getText().toString();
        String email = mEtEmail.getText().toString();
        String mobile = mEtPhone.getText().toString();
        String password = mEtPassword.getText().toString();
        String reEnterPassword = mEtRePassword.getText().toString();

        if (name.isEmpty() || name.length() < 3) {
            mTilName.setErrorEnabled(true);
            mTilName.setError("at least 3 characters");
            valid = false;
        } else {
            mTilName.setErrorEnabled(false);
            mEtName.setError(null);
        }

        if (address.isEmpty()) {
            mTilAddress.setErrorEnabled(true);
            mTilAddress.setError("Enter Valid Address");
            valid = false;
        } else {
            mTilAddress.setErrorEnabled(false);
            mTilAddress.setError(null);
        }

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mTilEmail.setErrorEnabled(true);
            mTilEmail.setError("enter a valid email address");
            valid = false;
        } else {
            mTilEmail.setErrorEnabled(false);
            mTilEmail.setError(null);
        }

        if (mobile.isEmpty() || mobile.length()!=10) {
            mTilPhone.setErrorEnabled(true);
            mTilPhone.setError("Enter Valid Mobile Number");
            valid = false;
        } else {
            mTilPhone.setErrorEnabled(false);
            mTilPhone.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            mTilPassword.setErrorEnabled(true);
            mTilPassword.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            mTilPassword.setErrorEnabled(false);
            mTilPassword.setError(null);
        }

        if (reEnterPassword.isEmpty() || reEnterPassword.length() < 4 || reEnterPassword.length() > 10 || !(reEnterPassword.equals(password))) {
            mTilRePassword.setErrorEnabled(true);
            mTilRePassword.setError("Password Do not match");
            valid = false;
        } else {
            mTilRePassword.setErrorEnabled(false);
            mTilRePassword.setError(null);
        }

        return valid;
    }

    public void onSignupSuccess() {
        mBtnSignup.setEnabled(true);
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    public void onSignupFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();
        mBtnSignup.setEnabled(true);
    }

}

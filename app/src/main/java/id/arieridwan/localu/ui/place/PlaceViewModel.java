package id.arieridwan.localu.ui.place;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import id.arieridwan.localu.data.dao.PlaceDao;
import id.arieridwan.localu.data.entity.PlaceEntity;
import id.arieridwan.localu.repository.PlaceRepository;
import id.arieridwan.localu.repository.PlaceRepositoryImpl;

public class PlaceViewModel extends ViewModel {

    PlaceRepository placeRepository;

    public PlaceViewModel() {
    }

    public void initViewModel(PlaceDao placeDao) {
        placeRepository = new PlaceRepositoryImpl(placeDao);
    }

    public LiveData<List<PlaceEntity>> getPlaces() {
        return placeRepository.getPlaces();
    }

}
